<?php
/**
 * Created by JetBrains PhpStorm.
 * User: aleks
 * Date: 14.04.13
 * Time: 21:52
 */

namespace Admingenerator\GeneratorBundle\Pagerfanta;

use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\ORM\EntityManager;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Seo\CounterBundle\Entity\RefererRepository;


class Adapter  implements AdapterInterface {

    /**
      * @var \Doctrine\ORM\Tools\Pagination\Paginator|\Pagerfanta\Adapter\DoctrineORM\Paginator
      */
     private $paginator;

     /**
      * Constructor.
      *
      * @param \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder $query A Doctrine ORM query or query builder.
      * @param Boolean $fetchJoinCollection Whether the query joins a collection (true by default).
      */
     public function __construct($query, $countQuery, $fetchJoinCollection = true)
     {
             $this->paginator = new Paginator($query, $countQuery, $fetchJoinCollection);
     }

     /**
      * Returns the query
      *
      * @return Query
      */
     public function getQuery()
     {
         return $this->paginator->getQuery();
     }

     /**
      * Returns whether the query joins a collection.
      *
      * @return Boolean Whether the query joins a collection.
      */
     public function getFetchJoinCollection()
     {
         return $this->paginator->getFetchJoinCollection();
     }

     /**
      * {@inheritdoc}
      */
     public function getNbResults()
     {
         return count($this->paginator);
     }

     /**
      * {@inheritdoc}
      */
     public function getSlice($offset, $length)
     {
         $this->paginator
             ->getQuery()
             ->setFirstResult($offset)
             ->setMaxResults($length);

         return $this->paginator->getIterator();
     }
}